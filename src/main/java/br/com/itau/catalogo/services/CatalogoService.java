package br.com.itau.catalogo.services;

import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.catalogo.models.Catalogo;
import br.com.itau.catalogo.repositories.CatalogoRepository;


@Service
public class CatalogoService {
	@Autowired
	CatalogoRepository catalogoRepository;
	
	public Iterable<Catalogo> listar(){
		return catalogoRepository.findAll();
	}
	
	public Optional<Catalogo> buscar(String nome) {
		return catalogoRepository.findById(nome);
	}
	
	public Catalogo cadastrar(Catalogo catalogo) {
		return catalogoRepository.save(catalogo);
	}
	
	@PostConstruct
	public void inicializarBase() {
		Catalogo catalogo1 = new Catalogo();
		catalogo1.setNome("Como reprovar 4 vezes em calculo By Matheus");
		catalogo1.setPreco(100.00);
		catalogo1.setDisponivel(true);
		
		Catalogo catalogo2 = new Catalogo();
		catalogo2.setNome("Aprendendo o BA ba");
		catalogo2.setPreco(100.00);
		catalogo2.setDisponivel(true);
		
		Catalogo catalogo3 = new Catalogo();
		catalogo3.setNome("Como comer todo o lanche antes do break");
		catalogo3.setPreco(100.00);
		catalogo3.setDisponivel(true);
		
		catalogoRepository.save(catalogo1);
		catalogoRepository.save(catalogo2);
		catalogoRepository.save(catalogo3);
	}
}
