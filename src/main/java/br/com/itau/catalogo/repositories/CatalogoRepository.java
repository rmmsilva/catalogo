package br.com.itau.catalogo.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.catalogo.models.Catalogo;


public interface CatalogoRepository extends CrudRepository<Catalogo, String> {

}
