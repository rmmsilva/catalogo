package br.com.itau.catalogo.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.catalogo.models.Catalogo;
import br.com.itau.catalogo.services.CatalogoService;


@RestController
public class CatalogoController {
	@Autowired
	CatalogoService catalogoService;
	
	@GetMapping
	public Iterable<Catalogo> listar(){
		return catalogoService.listar();
	}
	
	@GetMapping("/{nome}")
	public ResponseEntity buscar(@PathVariable String nome) {
		Optional<Catalogo> catalogoOptional = catalogoService.buscar(nome);
		
		if(catalogoOptional.isPresent()) {
			return ResponseEntity.ok(catalogoOptional.get());
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@PostMapping
	public ResponseEntity criar(@RequestBody Catalogo catalogo){
		catalogo = catalogoService.cadastrar(catalogo);
		
		return ResponseEntity.status(201).body(catalogo);
	}

}
